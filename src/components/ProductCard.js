import { Card, Button, Col, Row, Container} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

	const { _id, name, description, price, image } = productProp;



	return(
		
			
			
					<Card className="my-3" style={{ height: '38rem' }}>
						<Card.Img variant="top" src= {image} />

						<Card.Body>
							<Card.Title className="cardTitle"> { name } </Card.Title>
							
							

							<Card.Subtitle className="sub">Price:</Card.Subtitle>
							<Card.Text className="sub">$ { price } </Card.Text>

						
							<Button variant="danger" as={ Link } to={`/products/${_id}`}>Details</Button>

						</Card.Body>
					</Card>
			
		
			
		)
}



ProductCard.propTypes = {
	
	productProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		image: PropTypes.string.isRequired
	})

}