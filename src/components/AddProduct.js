import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';



export default function AddProduct({fetchData}) {

	//Add state for the forms of adding a course
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ image, setImage ] = useState();

	//states for opening and closing the modals
	const [ showAdd, setShowAdd ] = useState(false);

	//const [ file, setFile ] = useState('');

	//functions to handle opening and closing of our modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	//function for adding a course
	const addProduct = (e) => {
		e.preventDefault();
  
  	// use formdata instead of JSON for multer in backend
    const formData = new FormData()
    formData.append('image', image)
    formData.append('name', name)
    formData.append('description', description)
    formData.append('price', price)
		
		fetch('https://the-finest.herokuapp.com/products/add',{
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: formData
		})
		.then(data => {
			console.log(data)
			if(data){
				Swal.fire({
					title: 'G Pareh!',
					icon: 'success',
					text: 'Product successfully added!'
				})

				//Close our modal
				closeAdd()
				//you can use this as an alternative to refresh the whole document and get the updated data - window.location.reload()
				fetchData()
			} else {
				Swal.fire({
					title: 'Deins Pareh',
					icon: 'error',
					text: 'Try again'
				})

				fetchData()
			}

			//reset all states input
			setName('')
			setDescription('')
			setPrice(0)
			setImage('')
		})
	}

	return(
		<>
			<Button variant="primary" onClick={openAdd}>Add New Product</Button>

			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e =>addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
								type="text"
								required 
								value={name} 
								onChange={e => setName(e.target.value)} 
								/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
								type="text" 
								required 
								value={description} 
								onChange={e => setDescription(e.target.value)} 
								/>
						</Form.Group>	

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
								type="number" 
								required 
								value={price} 
								onChange={e => setPrice(e.target.value)} 
								/>
						</Form.Group>

						<Form.Group controlId="formFile" className="mb-3">
						    <Form.Label>Upload Image</Form.Label>
						    <Form.Control 
						    type="file" 
						    required
						    onChange={e => setImage(e.target.files[0])}
						    />
						</Form.Group>
					
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
		</>
		)
}