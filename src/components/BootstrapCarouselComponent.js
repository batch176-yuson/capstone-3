// src/components/bootstrap-carousel.component.js
import React from "react";
import { Carousel, Container, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import poster from '../images/poster.jpg';
import kaws from '../images/kaws.jpg';
import murakami from '../images/murakami.jpg';
import{ Link } from 'react-router-dom';
import Logo2 from '../images/logo2.png';
class BootstrapCarouselComponent extends React.Component {
render() {
return (
	<>
	<Container variant="dark"> 
		<Carousel>
		  <Carousel.Item interval={1000}>
		    <img
		      className="d-block w-100"
		      src={kaws}
		      alt="First slide"
		    />
		    <Carousel.Caption className="text-light Caption">
		      <h1>KAWS</h1>
		      <p className="font-weight-bold">Brooklyn-based artist KAWS has dominated the collectibles game for years. Shop all his top items include the BFF, Companion, & much more here.</p>
		      <Button as={Link} to="/products" variant="danger">SHOP NOW</Button>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item interval={500}>
		    <img
		      className="d-block w-100"
		      src={poster}
		      alt="Second slide"
		    />
		    <Carousel.Caption className="text-dark Caption">
		      <h1>Off-White Collection</h1>
		      <p>Virgil Abloh's take on Luxury Fashion including the Latest Collections of Shoes.</p>
		      <Button as={Link} to="/products" variant="danger">SHOP NOW</Button>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={murakami}
		      alt="Third slide"
		    />
		    <Carousel.Caption className="text-white Caption">
		      <h1>Takashi Murakami</h1>
		      <p>Japanese artist Takashi Murakami is known for his DOB character along with his colorful Flower Plushes.</p>
		      <Button as={Link} to="/products" variant="danger">SHOP NOW</Button>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>
	</Container>
	<div >
		<img className="mx-auto d-block my-5" 
		src={Logo2}
		width="300" />
	</div>
	</>
)
};
}
export default BootstrapCarouselComponent;