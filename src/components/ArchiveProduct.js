import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchivedProduct({product, isActive, fetchData}) {

	const archiveToggle = (productId) => {
		fetch(`
		https://the-finest.herokuapp.com/products/${productId}/archive`, {method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire ({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully disabled'
				})
				fetchData()
			} else {
				Swal.fire ({
					title: 'error',
					icon: 'error',
					text: 'Something went wrong'
				})
				fetchData()
			}
		})
	}

	const activateToggle = (productId) => {
		fetch(`
		https://the-finest.herokuapp.com/products/${productId}/activate`,{
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {
				Swal.fire ({
					title: 'Success',
					icon: 'success',
					text: 'Course successfully enabled'
				})
				fetchData()
			} else {
				Swal.fire ({
					title: 'error',
					icon: 'error',
					text: 'Something went wrong'
				})
				fetchData()
			}
		})
	}

	return(

		<>

			{isActive ?
				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Disable</Button>
				:

				<Button variant="success" size="sm" onClick={() => activateToggle(product)}>Enable</Button>
			}
			
		</>
		)
}