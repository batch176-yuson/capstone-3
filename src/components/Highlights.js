//React bootstrap components
import { Row, Col, Card } from 'react-bootstrap';
import { Button, Container } from 'react-bootstrap';
import  KAWS  from '../images/kawsholiday.jpg'
import MURAKAMI from '../images/murakamiplush.jpg';
import ABLOH from '../images/keepoff.jpg';

export default function Highlights(){
	return(
     
       <Container>
            <h1>FEATURED</h1>
		<Row className='my-5'>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3"> 
                <Card.Img variant="top" src= {KAWS} />
					<Card.Body>
						<Card.Title className="cardTitle">
							<h2>KAWS Holiday UK Vinyl Figure</h2>
						</Card.Title>
                        
						<Card.Text className="desc">
                        To go alongside a hot air balloon world tour, KAWS released a newly designed Companion figure titled Holiday UK. This is the first time a KAWS Holiday exhibit has graced the United Kingdom as the previous exhibits were located in Taipei, Seoul and Hong Kong. The 10 inch tall vinyl Companion figure is dressed from its "X" marked feet up to its head in shades of brown.
						</Card.Text>
                        <Card.Subtitle className="sub"><h4>Price: $445</h4></Card.Subtitle>
                        <Button variant="danger" className="highBut">Shop</Button>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3"> 
                <Card.Img variant="top" src= {MURAKAMI} />
					<Card.Body>
						<Card.Title className="cardTitle">
							<h2>Takashi Murakami Flower Plush 60 CM</h2>
						</Card.Title>
                        
						<Card.Text className="desc">
                        Takashi Murakami has a longstanding tradition of producing some of the cutest plush collectibles out there. His iconic flower character has been seen in galleries and homes all over the world, and this 60cm Rainbow Flower Plush is an affordable way to own a piece from the masterful Japanese artist. This particular plush features Murakami's classic smiling face surrounded by a rainbow of colors, and measures 60cm in diamater. Cop one today to brighten up any room.
						</Card.Text>
                        <Card.Subtitle className="sub"><h4>Price: $224</h4></Card.Subtitle>
                        <Button variant="danger" className="highBut">Shop</Button>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3"> 
                <Card.Img variant="top" src= {ABLOH} />
					<Card.Body>
						<Card.Title className="cardTitle">
							<h2>Virgil Abloh x IKEA "KEEP OFF" Rug 200x300 CM</h2>
						</Card.Title>
                        
						<Card.Text className="desc">
                        After being rumored as long ago as summer of 2017, Virgil Abloh's first collaborative piece with IKEA has finally released. As a part of IKEA's fifth IKEA Art Event, in which the company taps multidisciplinary artists to commision furniture pieces for them, Virgil has been tapped to design one of eight rugs to be released for the event. The rug features a persian rug with the words "KEEP OFF" between Virgil's signature quotation marks and was designed to depict the ironic nature of an overbearing parent who protects their furniture. He is quoted on the piece stating "I wanted an ironic take on the traditional attitude to furnishing where the living room is just a showroom, not somewhere you sit". The Rug was released Friday May 10th 2019, and retailed at $500 USD.
						</Card.Text>
                        <Card.Subtitle className="sub"><h4>Price: $2500</h4></Card.Subtitle>
                        <Button variant="danger" className="highBut">Shop</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
    
    </Container>
		)
}