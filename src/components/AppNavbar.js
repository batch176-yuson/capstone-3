import { useState, useContext } from 'react';
//React bootstrap components
import {Navbar, Nav} from 'react-bootstrap';

//react-router
import{ NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import logo1  from '../images/finestlogo.png';


export default function AppNavbar() {

	const { user } = useContext(UserContext);

	return(
		<Navbar expand="lg" className="mb-5 navuh">
			<Navbar.Brand className="ms-3" as={NavLink} to="/"	>
				<img
					alt=" "
					src= { logo1 }
					className= "Logo"
				/>
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" className="mx-3"/>
			<Navbar.Collapse id="basic-navbar-nav" className="ms-3">
				<Nav className ="ms-auto">
					<Nav.Link as={NavLink} to="/" 
					  style={({ isActive }) =>
					  isActive
						? {
							color: '#ea0a0a'
						  }
						: { color: '#000000'}}>Home</Nav.Link>
					<Nav.Link as={NavLink} to="/products" style={({ isActive }) =>
					  isActive
						? {
							color: '#ea0a0a'
						  }
						: { color: '#000000'}}>Products</Nav.Link>

					{(user.accessToken === null) ?
						<>
							<Nav.Link as={NavLink} to="/login" style={({ isActive }) =>
					  isActive
						? {
							color: '#ea0a0a'
						  }
						: { color: '#000000'}}>Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register" style={({ isActive }) =>
					  isActive
						? {
							color: '#ea0a0a'
						  }
						: { color: '#000000'}}>Register</Nav.Link>
						</>
						:
						<>
						{(user.isAdmin === true) ?
							<Nav.Link as={NavLink} to="/logout" style={({ isActive }) =>
							isActive
							  ? {
								  color: '#ea0a0a'
								}
							  : { color: '#000000'}}>Logout</Nav.Link>
						:
						<>
							<Nav.Link as={NavLink} to="/mycart" style={({ isActive }) =>
					  isActive
						? {
							color: '#ea0a0a'
						  }
						: { color: '#000000'}}>Cart</Nav.Link>
					    	<Nav.Link as={NavLink} to="/myorders" style={({ isActive }) =>
					  isActive
						? {
							color: '#ea0a0a'
						  }
						: { color: '#000000'}}>Orders</Nav.Link>
					    	<Nav.Link as={NavLink} to="/logout" style={({ isActive }) =>
					  isActive
						? {
							color: '#ea0a0a'
						  }
						: { color: '#000000'}}>Logout</Nav.Link>
						</>
					}
					</>
				}

					
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}