import { useState } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import ProductPage from './pages/ProductPage';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import SpecificProduct from './pages/SpecificProduct';
import CartView from './pages/CartView';
import OrdersPage from './pages/OrdersPage';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap';

import { BrowserRouter, Routes, Route} from 'react-router-dom';

function App() {
  
  const [ user, setUser ] = useState({
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

    const unsetUser = () => {
    localStorage.clear();
  }

  return (
    <>
      <UserProvider value = {{ user, setUser, unsetUser }}>
        <BrowserRouter>
            <AppNavbar />
            <Container>
                <Routes>
                  <Route path="/" element={ <Home /> }/>
                  <Route path="/products" element={ <ProductPage /> }/>
                  <Route path="/register" element={ <Register /> }/>
                  <Route path="/login" element={ <Login /> }/>
                  <Route path="/logout" element={ <Logout /> }/>
                  <Route path="/mycart" element={ <CartView /> }/>
                  <Route path="/myorders" element={ <OrdersPage /> }/>
                  <Route path="/products/:productId" element={ <SpecificProduct /> }/>
                  <Route path="*" element={ <ErrorPage /> }/>
                </Routes>
            </Container>
        </BrowserRouter>
      </UserProvider>  
    </>
  );
}

export default App;
