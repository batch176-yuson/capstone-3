import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');
	const [ passwordStatus, setPasswordStatus ] = useState('');
	const [ isButtonActive, setButtonActive ] = useState(true);

    

	useEffect(() => {
		//Validation to enable submit button
		if(email !== '' && password !== '' && verifyPassword !== ''){
			if (password !== verifyPassword) {
				setPasswordStatus("Password does not match!");
				setButtonActive(false);
			} else {
				setPasswordStatus("Password match!");
				setButtonActive(true);
			}

		}else {
			setButtonActive(false);
		}
	}, [email, password, verifyPassword, passwordStatus])

	function registerUser(e) {
		e.preventDefault();

			fetch('https://the-finest.herokuapp.com/users/register', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data.message)

				if(data){

					if(data.message === `Email already exists. Do you wish to login?`) {
						Swal.fire({
						title: 'error',
						icon: 'error',
						text: 'Email already existed. Do you wish to login?'
						})

					} else {
						Swal.fire({
				  		title: 'SHEEEEEEESH!',
				  		icon: 'success',
				 	 	text: `You are now Registered Pareh!`
				})
				navigate('/login')
					}

				} 
				else {
					Swal.fire({
						title: 'error',
						icon: 'error',
						text: 'Please try again'
					})
				}
		})
    }

	return(

    <div>
            <Form className="m-3 text-center" onSubmit={e => registerUser(e)}>
		<h1>Register</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control className="text-center"
					type="email"
					placeholder="Enter your email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted" >
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control className="text-center" 
					type="password"
					placeholder="Enter your password"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control className="text-center"
					type="password"
					placeholder="Verify password"
					required
					value={verifyPassword}
					onChange={e => setVerifyPassword(e.target.value)}
				/>
			</Form.Group>

			{isButtonActive ? 
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>
			}	
		</Form>
    </div>
		
		)
}