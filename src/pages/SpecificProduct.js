import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useParams, Link, useNavigate } from 'react-router-dom';
import CartView from '../pages/CartView';
//useParams() contains any values we are trying to pass in the URL stored
//useParams is how we receive the courseId passed via the URL

export default function SpecificProduct() {
	
	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [subTotal, setSubTotal] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [image, setImage] = useState();

	const [cart, setCart] = useState([]);


	useEffect(() => {

		fetch(`https://the-finest.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImage(data.image)
			
		})

		setSubTotal(quantity * price)

	}, [subTotal, quantity, cart])

	function decrementQuantity() {
		setQuantity(prevQuantity => prevQuantity - 1)
		if(quantity === 0) {
			setQuantity(0)
		}
	}

	function incrementQuantity() {
		setQuantity(prevQuantity => prevQuantity + 1)
	}


	const addToCart = (prodId) => {
		if (quantity === 0) {
			Swal.fire({
				title: 'Quantity cannot be zero Pareh!',
				icon: 'error'
			})
		} else {
			console.log(subTotal)
			let newCartProduct = {
				productId: prodId,
				name: name,
				price: price,
				image: image,
				quantity: quantity,
				subTotal: subTotal
			}

			console.log(newCartProduct)

			if(localStorage.getItem('cartproducts') == null) {
				localStorage.setItem('cartproducts', '[]')
			}

			let storedData = JSON.parse(localStorage.getItem('cartproducts'));
			let newCartArray = [];
			let isExisting = false;

			for (let i = 0; i < storedData.length; i++) {
				if (storedData[i].productId === prodId){
					storedData[i].quantity += quantity;
					storedData[i].subTotal += subTotal;
					isExisting = true
				}

				newCartArray.push(storedData[i])
			}

			if (isExisting === false) {
				newCartArray.push(newCartProduct)
			}

			localStorage.setItem('cartproducts',JSON.stringify(newCartArray));

			Swal.fire({
				title: 'Successfully added!',
				icon: 'success',
				text: `You have successfully added ${ name }`
			})
		}
	}



	const { user } = useContext(UserContext);



	return(
		<Container>
			<Card>
			<Card.Img variant="top" src= {image} />
				<Card.Header>
					<h4>{ name }</h4>
				</Card.Header>

				<Card.Body>
					<Card.Text>{ description }</Card.Text>
					<h6>Price: $ { price } </h6>

					<h6>Quantity:</h6>
					<Button variant="danger" onClick={decrementQuantity}>-</Button>
					<span> {quantity} </span>
					<Button  variant="danger" onClick={incrementQuantity}>+</Button>
					<h6>Subtotal: {subTotal}</h6>
				</Card.Body>

				<Card.Footer>
				{ user.accessToken !== null ?
					<>
					<Button variant="danger" onClick={() => addToCart(productId)}>Add To Cart</Button>
					<Button className="mx-1" variant="danger" as={ Link } to={`/mycart`}> View Cart</Button>
				
					</>
					:
					<Button variant="danger" as={ Link } to="/login">Login to Order</Button>
					
				 }
				</Card.Footer>
			</Card>
		</Container>

		)
}