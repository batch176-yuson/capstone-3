import BootstrapCarouselComponent from '../components/BootstrapCarouselComponent';
import Highlights from '../components/Highlights'




export default function Home() {
	return(
		<>
			<BootstrapCarouselComponent /> 
			<Highlights />
		</>
		)
}