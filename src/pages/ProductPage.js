import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import { useContext, useEffect, useState } from 'react';
import {Container} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function ProductPage(){

	const [ allProducts, setAllProducts ] = useState([])

	const fetchData = () => {
		fetch('https://the-finest.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setAllProducts(data)
		})
	}

	useEffect(() => {
		fetchData()
	},[])


	const { user } = useContext(UserContext);
	return(
		<>
			<h1 className ="mt-3 text-center">Products</h1>
			<Container>
			{(user.isAdmin === true)?

				<AdminView productsData={allProducts} fetchData={fetchData} />

				:

				<UserView productsData={allProducts} />

			} 
			</Container>
		</>

		)
}