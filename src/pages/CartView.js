import { useState, useEffect } from 'react';
import { Form, Table, Button, TextField } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function CartView() {

	const [cart, setCart] = useState([]);
	const [quantity, setQuantity] = useState('');
	const [name, setName] = useState('');
	const [grandTotal, setGrandTotal] = useState(0);
	const [storedData, setStoredData] = useState(JSON.parse(localStorage.getItem('cartproducts')));
	const navigate = useNavigate();

	useEffect(() => {
		//[incrementQuantity Fucntion]
				function incrementQuantity (prodId) {
					let updatedCartArray = [];
					for (let i = 0; i < storedData.length ; i++) {
						if (storedData[i].productId === prodId){
							storedData[i].quantity += 1;
							storedData[i].subTotal = storedData[i].quantity * storedData[i].price
						}
						updatedCartArray.push(storedData[i])
						
					}
					localStorage.setItem('cartproducts', JSON.stringify(updatedCartArray));
					window.location.reload(false);
				}
		//[decrementQuantity Fucntion]
				function decrementQuantity (prodId) {
					let updatedCartArray = [];
					for (let i = 0; i < storedData.length ; i++) {
						if (storedData[i].productId === prodId){
							if(storedData[i].quantity === 1) {
								storedData[i].quantity = 1
							} else {
								storedData[i].quantity -= 1;
								storedData[i].subTotal = storedData[i].quantity * storedData[i].price
							}
						}
						updatedCartArray.push(storedData[i])
					}
					localStorage.setItem('cartproducts', JSON.stringify(updatedCartArray));
					window.location.reload(false);
				}
		//[removeAnItem Function]
				function removeItem (prodId) {
					let updatedCartArray = [];
					for (let i = 0; i < storedData.length ; i++) {
						if (storedData[i].productId === prodId){
								continue;
							}
						updatedCartArray.push(storedData[i])
					}
					localStorage.setItem('cartproducts', JSON.stringify(updatedCartArray));
					window.location.reload(false);
				}

		if(localStorage.getItem('cartproducts') == null){
			localStorage.setItem('cartproducts', '[]')
			Swal.fire({
				title: 'Nothing Here yet. Wanna add a product?',
				icon: 'question',
				confirmButtonColor: "#b36b14",
			})

			navigate('/products')
		} else {
			const cartArr = storedData.map(cartproducts => {
					setGrandTotal(prevGrandTotal => prevGrandTotal + cartproducts.subTotal)
					return(
						<tr key={cartproducts.productId}>
							<td colSpan="8" className="bg-light">  {cartproducts.name}
							</td>
							<td className="bg-light text-dark"><span>$</span> {cartproducts.price}
							</td>
							<td>
							<Button className="mx-2" variant="dark" onClick={() => decrementQuantity(cartproducts.productId)}> - </Button>
									 <span> {` ${cartproducts.quantity} `} </span>
							<Button className="mx-2" variant="dark" onClick={() => incrementQuantity(cartproducts.productId)}> + </Button>

							</td>
							<td className="bg-light"><span>$</span>  {cartproducts.subTotal}
							</td>
							<td>
							<Button className="mx-2" variant="dark" onClick={() => removeItem(cartproducts.productId)}> Remove </Button>
							</td>

						</tr>
						)
			})
			setStoredData(JSON.parse(localStorage.getItem('cartproducts')))
			setCart(cartArr)
		}
	}, [quantity])

	//[ADD TO ORDER FUNCTION]
			const addToOrders = () => {
					if(storedData.length === 0) {
							Swal.fire({
							title: 'Nothing Here yet. Wanna to add a something?',
							icon: 'question',
							confirmButtonColor: "#b36b14",
						})
					} else {

						let newOrder = [];

						for(let i = 0; i < storedData.length; i++) {
							let cartItem = {
								productId: storedData[i].productId,
								quantity: storedData[i].quantity
							}
							newOrder.push(cartItem)
							
						}
							fetch('https://the-finest.herokuapp.com/orders/createorder', {
							method: 'POST',
							headers: {
								'Content-Type': 'application/json',
								Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
							},
							body: JSON.stringify(newOrder)
						})
						.then(res => res.json())
						.then(data => {

							if(data){

								Swal.fire({
								  position: 'top-end',
								  icon: 'success',
								  title: `You have successfully checkout all your cart items!`,
								  confirmButtonColor: "#b36b14",
								  showConfirmButton: false,
								  timer: 3000
								})
								localStorage.removeItem('cartproducts');
								navigate('/myorders')
							} else {
								Swal.fire({
									title: 'error!',
									icon: 'error',
									confirmButtonColor: "#b36b14",
									text: 'Something went wrong. Please try again :('
								})
							}
						})	
					
				}
			}


	return(
		<>
			<div className="my-4" >
				<h1> MY CART</h1>
			</div>
			
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-light">
					<tr>
						<th colSpan="8">NAME</th>
						<th>PRICE</th>
						<th>QUANTITY</th>
						<th>SUBTOTAL</th>
						<th>{` `}</th>
					</tr>
				</thead>

				<tbody>
					{ cart }
				</tbody>
			</Table>
			<Button className="p-3"  variant="primary" style={{float: "right", fontWeight: 'bold'}} onClick={() => addToOrders(cart)}> CHECKOUT </Button>
			<div className="bg-dark text-light mx-2 p-2" style={{fontWeight: 'bold' ,float: "right"}}> <h3>{`𝗚𝗿𝗮𝗻𝗱 𝗧𝗼𝘁𝗮𝗹: `} <span>$</span > {grandTotal} </h3>  </div>
		</>

		)
}